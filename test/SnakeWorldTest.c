/**
 * Tests of SnakeWorld. 
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "SnakeWorld.c"


void testGetSnake_X() {
    // Given
	 World snakeWorld = newWorld("SnakeWorld");

	    // When
	    Actor snake = getSnake(snakeWorld);

	    // Then
		assertEquals_int(0 ,getX(snake));
}

void testGetSnake_Y() {
    // Given
	World snakeWorld = newWorld("SnakeWorld");

		    // When
		    Actor snake = getSnake(snakeWorld);

		    // Then
			assertEquals_int(4 ,getY(snake));
}
