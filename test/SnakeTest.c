/**
 * Tests of Snake. 
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "SnakeWorld.c"
#include "Snake.c"

void testImage() {
    // Given
    Actor snake = newActor("Snake");
    // When
    const char* imageFile = getImageFile(snake);
    // Then
    assertEquals_String("brown_snake.png", imageFile);
}

void testImageWidth() {
    // Given
	Actor snake = newActor("Snake");
    // When
    int width = getImageWidth(snake);
    // Then
    assertEquals_int(50, width);
}

void testImageHeight() {
    // Given
	Actor snake = newActor("Snake");
    // When
	int height = getImageHeight(snake);
	// Then
	assertEquals_int(50, height);
}

void testAct_X() {
    // Given
        World snakeworld = newWorld("SnakeWorld");
	    Actor snake = getSnake(snakeworld);

	    // When
	    actSnake(snake);

	    // Then
	    assertEquals_int(1, getX(snake));
}

void testAct_Y() {
    // Given
    World snakeworld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeworld);

    // When
    actSnake(snake);

    // Then
    assertEquals_int(2, getY(snake));
}

void testActAfterSetDistance_X() {
    // Given
	World snakeworld = newWorld("SnakeWorld");
	Actor snake = getSnake(snakeworld);
    // When
	setDistance(snake, 3);
	actSnake(snake);
    // Then
	assertEquals_int(1, getX(snake));
}
    
void testActAfterSetDistance_Y() {
    // Given
	World snakeworld = newWorld("SnakeWorld");
	Actor snake = getSnake(snakeworld);
    // When
	setDistance(snake, 3);
	actSnake(snake);
    // Then
	assertEquals_int(1, getY(snake));
}
    
void testActAfterSetAngle_X() {
    // Given
	World snakeworld = newWorld("SnakeWorld");
    Actor snake = getSnake(snakeworld);
	// When
    setAngle(snake, 320);
	actSnake(snake);
	// Then
    assertEquals_int(2, getX(snake));
}
    
void testActAfterSetAngle_Y() {
    // Given
	World snakeworld = newWorld("SnakeWorld");
	Actor snake = getSnake(snakeworld);
	// When
	setAngle(snake, 320);
    actSnake(snake);
    // Then
    assertEquals_int(3, getY(snake));
}
    
void testActAfterSetDistanceAngle_X() {
    // Given
	World snakeworld = newWorld("SnakeWorld");
	Actor snake = getSnake(snakeworld);
    // When
	setAngle(snake, 275);
    setDistance(snake,4);
	actSnake(snake);
    // Then
	assertEquals_int(0, getX(snake));
}

void testActAfterSetDistanceAngle_Y() {
    // Given
	World snakeworld = newWorld("SnakeWorld");
	Actor snake = getSnake(snakeworld);
    // When
	setAngle(snake, 275);
	setDistance(snake,4);
	actSnake(snake);
    // Then
	assertEquals_int(0, getY(snake));
}
