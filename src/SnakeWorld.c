/**
 * The SnakeWorld.c file defines a scenario of 11 cells on the X axis by 5 cells 
 * on the Y axis, where each cell is drawn using a 60x60 size image built from 
 * the "sand.jpg" file. When a scenario is executed, Greenfoot executes the act() 
 * function of all the actors repeatedly, that is, the execution of a Greenfoot 
 * scenario is a sequence of cycles and in each cycle the act () function of all 
 * the actors it contains is executed scenario.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

#define _width    11
#define _height   5
#define _cellSize 60

/**
 * Initialize its background image.
 */
void startSnakeWorld(World snakeWorld) {
    setBackgroundFile(snakeWorld, "sand.jpg");
    Actor snake = newActor("Snake");
    addActorToWorld(snakeWorld, snake, 0, 4);
    setGlobalActor(snakeWorld, "snake", snake);

}

Actor getSnake(World snakeWorld) {
    return getGlobalActor(snakeWorld, "snake");
}
