/**
 * The Snake.c file defines a game actor that has a 50x40 size image built 
 * from the "snake.png" file. The actSnake() method declared in the Snake.c  
 * file defines the snake behaviour in each cycle of the scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image.
 */
void startSnake(Actor snake) {
	setImageFile(snake, "brown_snake.png");
	setImageScale(snake, 50, 50);
	setGlobalInt(snake, "distance", 2);
	setGlobalInt(snake, "angle", 290);
}

void actSnake(Actor snake) {
	turn(snake,getGlobalInt(snake, "angle"));
	move(snake,getGlobalInt(snake, "distance"));
}

void setAngle(Actor snake, int angle) {
	setGlobalInt(snake, "angle", angle);
}

void setDistance(Actor snake, int distance) {
	 setGlobalInt(snake, "distance", distance);
    
}
