/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>

#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
    #define LibraryFrame "java -cp \".;.jni;.jar/org.junit_4.13.2.v20211018-1956.jar;.jar/org.hamcrest.core_1.3.0.v20180420-1519.jar;.jar/turnbased.jar;.jar/greenfoot.jar;.jar/bluejcore.jar;.jar/rsyntaxtextarea-3.3.3.jar;.jar/GreenfootFrame.jar\" --module-path \"%JAVAFX_LIB%\" --add-modules javafx.base,javafx.controls,javafx.fxml,javafx.graphics,javafx.media,javafx.swing,javafx.web,javafx.swt  GreenfootFrame"
#else
    #define LibraryFrame "java -cp \".:.jni:.jar/org.junit_4.13.2.v20211018-1956.jar:.jar/org.hamcrest.core_1.3.0.v20180420-1519.jar:.jar/turnbased.jar:.jar/greenfoot.jar:.jar/bluejcore.jar:.jar/rsyntaxtextarea-3.3.3.jar:.jar/GreenfootFrame.jar\" --module-path $JAVAFX_LIB --add-modules javafx.base,javafx.controls,javafx.fxml,javafx.graphics,javafx.media,javafx.swing,javafx.web,javafx.swt  GreenfootFrame"
#endif

int main(void) {
	system(LibraryFrame);
	return 0;
}
